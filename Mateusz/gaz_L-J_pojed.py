# -*- coding: utf-8 -*-
import random as ran
import numpy as np
import math
import matplotlib.pyplot as plt

class Kulka():
    def __init__(self, pol, m=0.01, v=[0,0],bound=[],f=[0,0,0,0],F=[0,0]):
        self.__polozenie = [pol]
        self.__masa = m
        self.__v = [v]
        self.__bound = bound
        self.__f=f
        self.__F=F
        
    @property
    def F_get(self):
        F = self.__F
        return F

    def F_set(self,k, new_F):
        self.__F[k]=new_F
        
    @property
    def f_get(self):
        f = self.__f
        return f

    def f_set(self,i, new_f):
        self.__f[i]=new_f

    @property
    def pos_get(self):
        pozycja = self.__polozenie[-1]
        return pozycja
    
    @property
    def bound_get(self):
        bound = self.__bound
        return bound

    def bound_set(self, new_bound):
        self.__bound=new_bound

    def bound_rm(self,rm_pose):
        self.__bound=self.__bound.remove(rm_pose)

    def pos_set(self, new_pos):
        self.__polozenie.append(new_pos)
    
    # sprawdzam jak przemieszcza sie kulka
    @property
    def pos_get_all(self):
        pozycja = self.__polozenie
        return pozycja
    
    @property
    def ver_get(self):
        ver = self.__v[-1]
        return ver
    @property
    def ver_get_all(self):
        vr = self.__v
        return vr
    @property
    def m_get(self):
        m = self.__masa
        return m
    
    def ver_set(self, new_ver):
        self.__v.append(new_ver)
        
class Uklad():
    def __init__(self, lista_kulek,T=273):
        self.__T=T
        self.__lista_kulek = lista_kulek
        self.__powiazania =[]
        self.__uklad_kul=[]
        self.__n=int((len(lista_kulek))**(1/2))
        for i in range(self.__n):
            helper=[]
            helpero=[]
            for j in range(self.__n):
                helper.append(0)
                helpero.append(self.__lista_kulek[i*self.__n+j])
            self.__powiazania.append(helper)
            self.__uklad_kul.append(helpero)
        self.__uklad=[]   
# tworzenie sieci n x n
        for i in range (self.__n):
            pomoc=[]
            for j in range (self.__n):
                k=self.__n*i+j
                pomoc.append(self.__lista_kulek[k])
            self.__uklad.append(pomoc)
            
# okreslanie wiezi w sieci n x n z zamknieciem przestrzeni
        for i in range (self.__n):
            for j in range (self.__n):
                if i==0 and j>0 and j<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([self.__uklad_kul[self.__n-1][j],self.__uklad_kul[i+1][j],self.__uklad_kul[i][j+1],self.__uklad_kul[i][j-1]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif j==0 and i>0 and i<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([self.__uklad_kul[i-1][j],self.__uklad_kul[i+1][j],self.__uklad_kul[i][j+1],self.__uklad_kul[i][self.__n-1]]) 
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    
                elif j==(self.__n-1) and i>0 and i<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([self.__uklad_kul[i-1][j],self.__uklad_kul[i+1][j],self.__uklad_kul[i][0],self.__uklad_kul[i][j-1]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif i==(self.__n-1) and j>0 and j<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([self.__uklad_kul[i-1][j],self.__uklad_kul[0][j],self.__uklad_kul[i][j+1],self.__uklad_kul[i][j-1]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif j==0 and i==0:
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([self.__uklad_kul[self.__n-1][j],self.__uklad_kul[i+1][j],self.__uklad_kul[i][j+1],self.__uklad_kul[i][self.__n-1]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    

                elif j==0 and i==(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([self.__uklad_kul[i-1][j],self.__uklad_kul[0][j],self.__uklad_kul[i][j+1],self.__uklad_kul[i][self.__n-1]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif i==0 and j==(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([self.__uklad_kul[self.__n-1][j],self.__uklad_kul[i+1][j],self.__uklad_kul[i][0],self.__uklad_kul[i][j-1]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif i==(self.__n-1) and j==(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([self.__uklad_kul[i-1][j],self.__uklad_kul[0][j],self.__uklad_kul[i][0],self.__uklad_kul[i][j-1]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                else:
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([self.__uklad_kul[i-1][j],self.__uklad_kul[i][j-1],self.__uklad_kul[i][j+1],self.__uklad_kul[i+1][j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
 
    @property    
    def powiazania_get(self):
        powiazania = self.__powiazania
        return powiazania
    @property    
    def T_get(self):
        t = self.__T
        return t
    @property    
    def uklad_kul_get(self):
        uklad_kul = self.__uklad_kul
        return uklad_kul
  
    @property    
    def uklad_get(self):
        ukl = self.__uklad
        return ukl
   
    @property    
    def kulka_get(self):
        kul = self.__lista_kulek
        return kul


class Algorytmy:
    def update_pose(self):
        pass
    def update_ver(self):
        pass
    
class LeapFrog(Algorytmy):
    def __init__(self, dt=0.02):
        self.__tarcie = 0.9
        self.__dt = dt
    def update_pos(self, new_pos, kulka):
     #   print(kulka.pos_get,"stara")
        kulka.pos_set(new_pos)
     #   print(kulka.pos_get,"nowa")
    
    def update_ver(self, new_ver, kulka):
  #      print(kulka.ver_get,"stara")
        kulka.ver_set(new_ver)
   #     print(kulka.ver_get,"nowa")
      
    def ruch(self, uklad_kul):
#       petla wykonywana dla kazdej kulki
        for i in range (len(uklad_kul)):
            for j in range (len(uklad_kul)):
           #     print(f[i][j])
                new_ver_x = uklad_kul[i][j].ver_get[0] + uklad_kul[i][j].F_get[0]*self.__dt
                new_ver_y = uklad_kul[i][j].ver_get[1] + uklad_kul[i][j].F_get[1]*self.__dt
                new_pos_x = uklad_kul[i][j].pos_get[0] + new_ver_x*self.__dt
                new_pos_y = uklad_kul[i][j].pos_get[1] + new_ver_y*self.__dt

                if new_pos_x<0:
                    new_pos_x=new_pos_x*(-1)
                    r=new_pos_x%len(uklad_kul)
                    new_pos_x=(new_pos_x - r)/len(uklad_kul)+r
                    
                elif new_pos_x>len(uklad_kul):
                     r=new_pos_x%len(uklad_kul)
                     new_pos_x=(new_pos_x - r)/len(uklad_kul)+r
                else:
                    pass
                
                if new_pos_y<0:
                    new_pos_y=new_pos_y*(-1)
                    r=new_pos_y%len(uklad_kul)
                    new_pos_y=(new_pos_y - r)/len(uklad_kul)+r
                    
                elif new_pos_y>len(uklad_kul):
                    r=new_pos_y%len(uklad_kul)
                    new_pos_y=(new_pos_y - r)/len(uklad_kul)+r
                else:
                    pass
                
                new_ver=[new_ver_x,new_ver_y]
                new_pos=[new_pos_x,new_pos_y]                
                self.update_ver(new_ver, uklad_kul[i][j])
                self.update_pos(new_pos, uklad_kul[i][j])
                

class Potencjal:
    def calc_energy():
        pass
    def calc_forces():
        pass
    
class Harmoniczny(Potencjal):
    def __init__(self, k=1, x0=0):
        self.__k = k
        self.__x0 = x0
    
    def calc_energy(self,x):
        return (self.__k/2)*(x - self.__x0)**2
    
    def calc_forces(self,uklad_kul,powiazania):
        
#        for i in range (len(uklad_kul)):
#            for j in range(len(uklad_kul)):
#                for k in range (4):               
#                    print(uklad_kul[i][j].bound_get[k].pos_get)
#                print("/n")

        for i in range (len(uklad_kul)):
            for j in range (len(uklad_kul)):
                k1=uklad_kul[i][j].pos_get
#                print(powiazania[i])
                for k in range(4):
                    if k==0 or k==2:
#                        print(k1,k)
                        k2=uklad_kul[i][j].bound_get
                        a,b=k2[k].pos_get
#                        if k==2:
#                            print(k1,uklad_kul[i][j].bound_get[k].pos_get)
            #            print(a,b)
                        x = k1[0]-a
                        y = k1[1]-b
                        dx,dy=x,y
                        if x <0:
                            dx=-x
                        if y <0:
                            dy=-y
                        #d = np.linalg.norm(x)
                        if dx==0:
                            vx=1
                        else:
                            vx = x / dx
                        if dy==0:
                            vy=1
                        else:
                            vy = y / dy 
                        #f1 = -self.__k*(d - self.__x0)*v
                        f2x = self.__k*(dx - self.__x0)*vx
#           #             print(self.__k,dx,self.__x0,vx,f2x)
                        f2y = self.__k*(dy - self.__x0)*vy
                        f2=[f2x,f2y]
                        f1=[-f2x,-f2y]
                        uklad_kul[i][j].f_set(k,f2)
                        for www in range (len(uklad_kul)):
                            for aaa in range (len(uklad_kul)):
                                if uklad_kul[www][aaa].pos_get==[a,b]:
                                    uklad_kul[www][aaa].f_set(k+1,f1)
                                    
#        for i in range (len(uklad_kul)):
#            for j in range(len(uklad_kul)):              
#                print(uklad_kul[i][j].F_get)

            
        force=[] 
        x=0
        y=0      
        for i in range (len(uklad_kul)):
            H=[]
            for j in range (len(uklad_kul)):
                
                fi=uklad_kul[i][j].f_get
     #           print(fi,"lolo")
                for k in range(4):
                    x+=fi[k][0]
                    y+=fi[k][1]
     #               print(x,y,"lkasklsalk")
                H.append([x,y])
    #            print(H,"1")
                x=0
                y=0
     #       print(H,"2")
            force.append(H)
        return force

class Langevine(Potencjal):
    def __init__(self,T=300,tarcie=0.9999):
        self.__tarcie=tarcie
        self.__T=T
    def calc_energy(self,uklad_kul):
        e=0
        for i in range(len(uklad_kul)):
            for j in range(len(uklad_kul)):
                e+=(1/2)*uklad_kul[i][j].m_get*uklad_kul[i][j].ver_get[0]
                e+=(1/2)*uklad_kul[i][j].m_get*uklad_kul[i][j].ver_get[1]
        
        return e
        
    def calc_forces(self,uklad_kul):
        
        f=[]
        for k in range (len(uklad_kul)):
            g=[]
            for l in range (len(uklad_kul)):
                g.append(0)
            f.append(g)
        
        for i in range(len(uklad_kul)):
            for j in range(len(uklad_kul)):
                f[i][j]=[(2*self.__tarcie*self.__T)**(1/2)*ran.gauss(0,1),(2*self.__tarcie*self.__T)**(1/2)*ran.gauss(0,1)]
        return f
    
class LJ(Potencjal):
    def __init__(self,E=0.1,r0=0.1):
        self.__E=E
        self.__r0=r0
        
    def calc_energy(self,r):
        if r==0:
            return r
        else:
            V=-4*(self.__r0**6/r**6 - self.__r0**12/r**12)*self.__E
            return V
    def calc_forces(self,lista_kul):
        f=[]
        for k in range (len(lista_kul)):
            g=[]
            for l in range (len(lista_kul)):
                g.append(0)
            f.append(g)
        
        for k in range (len(lista_kul)):
               for l in range (len(lista_kul)):
                   fx=0
                   fy=0
                   for a in range (len(lista_kul)):
                       for b in range (len(lista_kul)):
                           if a==k and l==b:
                               pass
                           else:
                               rx=lista_kul[k][l].pos_get[0] - lista_kul[a][b].pos_get[0]
                               ry=lista_kul[k][l].pos_get[1] - lista_kul[a][b].pos_get[1]
                               if rx==0:
                                   fx+=0
                               else:
                                   fx+=-4*(-6*self.__r0**6/rx**(-7) + 12*self.__r0**12/rx**(-13))*self.__E
                               if ry==0:
                                   fy+=0
                               else:
                                   fy+=-4*(-6*self.__r0**6/ry**(-7) + 12*self.__r0**12/ry**(-13))*self.__E
                                   
                   f[k][l]=[fx,fy]
        return f

class Symulacje():
    def __init__(self, uklad, potencjal, algorytm, steps = 100):
        self.__uklad = uklad
        self.__potencjal = potencjal
        self.__algorytm = algorytm
        self.__steps = steps
        
    def run(self):
        lista_kul = self.__uklad.uklad_kul_get
        powiazania = self.__uklad.powiazania_get
        Energia=[]
        for i in range(self.__steps):
            f1 = self.__potencjal[0].calc_forces(lista_kul,powiazania)            
            f2 = self.__potencjal[1].calc_forces(lista_kul)            
            f3= self.__potencjal[2].calc_forces(lista_kul)
#            print(f1,f2,f3)
                   
            en=0
           
            for k in range (len(lista_kul)):
               for l in range (len(lista_kul)):
                   for a in range (len(lista_kul)):
                       for b in range (len(lista_kul)):
                           if a==k and l==b:
                               pass
                           else:
                               x=lista_kul[k][l].pos_get[0] - lista_kul[a][b].pos_get[0]
                               y=lista_kul[k][l].pos_get[1] - lista_kul[a][b].pos_get[1]
                               e1_x=self.__potencjal[0].calc_energy(x)
                               e1_y=self.__potencjal[0].calc_energy(y)
                               
                               e2_x=self.__potencjal[2].calc_energy(x)
                               e2_y=self.__potencjal[2].calc_energy(y)
                               en+=e1_x + e1_y + e2_x + e2_y
            el=self.__potencjal[1].calc_energy(lista_kul)
 #                   print(e1_x , e1_y , e2_x,e2_y)
#            print(f1,f2,f3)
            x=[]
            y=[]
            for k in range (len(lista_kul)):
               for l in range (len(lista_kul)):
                   x.append(f1[k][l][0]+f2[k][l][0]+f3[k][l][0])
                   y.append(f1[k][l][1]+f2[k][l][1]+f3[k][l][1])
                   
            for p in range (len(lista_kul)):
                for q in range(len(lista_kul)):              
                    lista_kul[k][l].F_set(0,x[p*len(lista_kul)+q])
                    lista_kul[k][l].F_set(1,y[p*len(lista_kul)+q])
#                    print(lista_kul[p][q].F_get)
#                       print(f[k][l],"lpp")
#            print("/n",f,"/n")
            Energia.append((en/2)+el)
#            print(f,"jkadsfifkjoafofwa")
            self.__algorytm.ruch(lista_kul)
        # testtuje
        
        return Energia
    
if __name__ == "__main__":
    kulki=[]
    x=5
    #x=int(input("wpisz długosc boku siatki: "))
    for i in range(x**2):
        kulka1 = Kulka([1])
        kulki.append(kulka1)
    uklad = Uklad(kulki)
    #print(uklad.powiazania_get)
    #print(uklad.uklad_kul_get)
    X1=[]
    Y1=[]
    for i in range (200,350,5):
        potencjal = [Harmoniczny(k=2, x0=2.2),Langevine(i),LJ()]
        algorytm = LeapFrog()
        s = Symulacje(uklad, potencjal, algorytm)
        E1=s.run()
        x=sum(E1)/len(E1)
#        print(x)
        
        X1.append(x)
        Y1.append(i)

    plt.plot(Y1, X1)
    plt.show()

