import random as ran
import numpy as np
import matplotlib.pyplot as plt

class Kulka():
    def __init__(self, pol, m=0.01,bound=[]):
        self.__polozenie = [pol]
        self.__masa = m
        self.__bound = bound
 

    @property
    def pos_get(self):
        pozycja = self.__polozenie[-1]
        return pozycja
    
    @property
    def bound_get(self):
        bound = self.__bound
        return bound

    def bound_set(self, new_bound):
        self.__bound=new_bound

    def bound_rm(self,rm_pose):
        self.__bound=self.__bound.remove(rm_pose)

    def pos_set(self, new_pos):
        self.__polozenie.append(new_pos)
    
    # sprawdzam jak przemieszcza sie kulka
    @property
    def pos_get_all(self):
        pozycja = self.__polozenie
        return pozycja

class Uklad():
    def __init__(self, lista_kulek,m):
        self.__m=m #dlugosc polimeru
        self.__lista_mer=[]
        self.__lista_kulek = lista_kulek
        self.__powiazania =[]
        self.__n=int((len(lista_kulek))**(1/2))
        for i in range(self.__n):
            helper=[]
            for j in range(self.__n):
                helper.append(0)
            self.__powiazania.append(helper)
        self.__uklad=[]   
# tworzenie sieci n x n
        for i in range (self.__n):
            pomoc=[]
            for j in range (self.__n):
                k=self.__n*i+j
                pomoc.append(self.__lista_kulek[k])
            self.__uklad.append(pomoc)
            
# okreslanie wiezi w sieci n x n z zamknieciem przestrzeni
        
        for i in range (self.__n):
            for j in range (self.__n):
                if i==0 and j>0 and j<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[self.__n-1,j],[i,j-1],[i,j+1],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif j==0 and i>0 and i<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,self.__n-1],[i,j+1],[i+1,j]]) 
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    
                elif j==(self.__n-1) and i>0 and i<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,j-1],[i,0],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif i==(self.__n-1) and j>0 and j<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,j-1],[i,j+1],[0,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif j==0 and i==0:
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[self.__n-1,j],[i,self.__n-1],[i,j+1],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    

                elif j==0 and i==(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,self.__n-1],[i,j+1],[0,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif i==0 and j==(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[self.__n-1,j],[i,j-1],[i,0],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif i==(self.__n-1) and j==(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,j-1],[i,0],[0,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                else:
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,j-1],[i,j+1],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
        #ustawienie poczatkowe polimeru
        x=ran.randint(5,self.__n-10)
        y=ran.randint(5,self.__n-10)
        self.__lista_mer.append(self.__uklad[x][y])
 
        while len(self.__lista_mer)<m:
            rand=ran.randint(0,3)
#            print(self.__lista_mer[-1].pos_get)
#            
#            print("rand",rand)
            
            if rand==0: #gora
                wsp=[x+1,y]
                for k in range (self.__n):
                    for l in range (self.__n):
                        if self.__uklad[k][l].pos_get==wsp:
                            ziomek=self.__uklad[k][l]
                ok=0            
                for j in range(len(self.__lista_mer)):
                    if self.__lista_mer[j].pos_get!=ziomek.pos_get:
                        ok+=1
                if ok==len(self.__lista_mer):
                    self.__lista_mer.append(ziomek)
                    x=wsp[0]
                    y=wsp[1]
                    
                       
                        
            elif rand==1: #dol
                wsp=[x-1,y]
                for k in range (self.__n):
                    for l in range (self.__n):
                        if self.__uklad[k][l].pos_get==wsp:
                            ziomek=self.__uklad[k][l]
                ok=0            
                for j in range(len(self.__lista_mer)):
                    if self.__lista_mer[j].pos_get!=ziomek.pos_get:
                        ok+=1
                if ok==len(self.__lista_mer):
                    self.__lista_mer.append(ziomek)
                    x=wsp[0]
                    y=wsp[1]
                    
            elif rand==2: #prawo
                wsp=[x,y+1]
                for k in range (self.__n):
                    for l in range (self.__n):
                        if self.__uklad[k][l].pos_get==wsp:
                            ziomek=self.__uklad[k][l]
                ok=0            
                for j in range(len(self.__lista_mer)):
                    if self.__lista_mer[j].pos_get!=ziomek.pos_get:
                        ok+=1
                if ok==len(self.__lista_mer):
                    self.__lista_mer.append(ziomek)
                    x=wsp[0]
                    y=wsp[1]
                    
            elif rand==3: #lewo
                wsp=[x,y-1]
                for k in range (self.__n):
                    for l in range (self.__n):
                        if self.__uklad[k][l].pos_get==wsp:
                            ziomek=self.__uklad[k][l]
                ok=0            
                for j in range(len(self.__lista_mer)):
                    if self.__lista_mer[j].pos_get!=ziomek.pos_get:
                        ok+=1
                if ok==len(self.__lista_mer):
                    self.__lista_mer.append(ziomek)
                    x=wsp[0]
                    y=wsp[1]

#        polimer - pokazywanie wspolrzednych kolejnych elementow
#        print("elp")
#        p=[]
#        q=[]
#        for i in range(len(self.__lista_mer)):
#            print(self.__lista_mer[i].pos_get)
#            p.append(self.__lista_mer[i].pos_get[0])
#            q.append(self.__lista_mer[i].pos_get[1])
#        plt.plot(p,q,'ro')
#        plt.axis([0,20,0,20])
#        plt.show()
#        for i in range (self.__n):
#            for j in range (self.__n):
#                print(self.__uklad[i][j].pos_get)
    
    @property    
    def powiazania_get(self):
        powiazania = self.__powiazania
        return powiazania
    
    @property    
    def T_get(self):
        t = self.__T
        return t
  
    @property    
    def uklad_get(self):
        ukl = self.__uklad
        return ukl
   
    @property    
    def kulka_get(self,a,b):
        kul = self.__uklad[a][b]
        return kul
    
    @property    
    def mer_get(self):
        mery = self.__lista_mer
        return mery
    
    def mer_set(self,new_mer):
        self.__lista_mer=new_mer

class Potencjal():
    def calc_en(self):
        pass
    
class Macierz_powiazan(Potencjal):
    
    def __init__(self):
        pass
                
    def calc_en(self,lista_mer):
#        print("elo")
        e0=(len(lista_mer)-1)*2
        e1=(len(lista_mer)-1)*2
        self.__macierz=[]
        for i in range(len(lista_mer)):
            l=[]
            for j in range(len(lista_mer)):
                l.append(0)
            self.__macierz.append(l)
        
        for i in range(len(lista_mer)):
            for j in range(len(lista_mer)):
                if i!= j and i!=j+1 and i!=j-1:               
                    if lista_mer[i].pos_get[0]-lista_mer[j].pos_get[0]<=1 and lista_mer[i].pos_get[0]-lista_mer[j].pos_get[0]>=-1 and lista_mer[i].pos_get[1]-lista_mer[j].pos_get[1]<=1 and lista_mer[i].pos_get[1]-lista_mer[j].pos_get[1]>=-1:
                        e1+=1
        e=e1-e0
        return e
    
    @property    
    def macierz_get(self):
        mac = self.__macierz
        return mac
    
class Ruch():
    def __init__(self):
        pass
    
    def ruchy(self,lista_mer,ukl,x,potencjal,T):
#        print(x,"jnfjjifeaijefojjeoiojifeoiejoijeroijesjiesjejfoiaoifjaoifjoifjoijffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff")
        uklad=ukl.uklad_get
        if x==0 or x==len(lista_mer)-1:
            y=ran.randint(1,2)
       
            if y==1:
                # ruch ogona
                old_pose=lista_mer[x].pos_get
                
                new_pose=[[old_pose[0],old_pose[1]-1],[old_pose[0],old_pose[1]+1],[old_pose[0]-1,old_pose[1]],[old_pose[0]+1,old_pose[1]]]
#                print(old_pose,new_pose)
                j=3
                k=0
                while len(new_pose)>1:
                    for i in range(len(lista_mer)):
  #                      print(i,len(lista_mer),j,len(new_pose))
                        if lista_mer[i].pos_get==new_pose[j]:
    #                        print(new_pose)
                            new_pose.remove(new_pose[j])
   #                         print(new_pose)
                            j=j-1
                    k+=1
                    if k==10:
 #                       print(k)
                        break
                for i in range(len(lista_mer)):
                        if lista_mer[i].pos_get==new_pose[0]:
                            new_pose.remove(new_pose[0])
                if len(new_pose)==0:
                    pass
                else:
    #                print("koko")
                    xd=ran.randint(0,len(new_pose)-1)
                    
                    x=new_pose[xd]
                    for i in range (len(lista_mer)):
                        if i%2==0:
                            for op in range(len(uklad)):
                                for opa in range (len(uklad)):
                                    if lista_mer[i].pos_get==uklad[op][opa].pos_get:
                                        q=uklad[op][opa].pos_get
                            p1=x[0]
                            p2=x[1]
                            lista_mer[i]=uklad[p1][p2]
                        else:
                            for op in range(len(uklad)):
                               for opa in range (len(uklad)):
                                   if lista_mer[i].pos_get==uklad[op][opa].pos_get:
                                       x=uklad[op][opa].pos_get
    
                            p1=q[0]
                            p2=q[1]
                            lista_mer[i]=uklad[p1][p2]
                
            else:
                lista_mery=[]
                for i in range (len(lista_mer)):
                    lista_mery.append(0)
                for i in range (len(lista_mer)):
                    lista_mery[i]=lista_mer[len(lista_mer)-1-i]
                old_pose=lista_mery[x].pos_get              
                new_pose=[[old_pose[0],old_pose[1]-1],[old_pose[0],old_pose[1]+1],[old_pose[0]-1,old_pose[1]],[old_pose[0]+1,old_pose[1]]]
               
                #przemieszczenie mera do przodu
#                war=len(lista_mer)-1-x
#                old_pose=lista_mer[war].pos_get
#                new_pose=[[old_pose[0],old_pose[1]-1],[old_pose[0],old_pose[1]+1],[old_pose[0]-1,old_pose[1]],[old_pose[0]+1,old_pose[1]]]
                j=3
                k=0
                while len(new_pose)>1:
                    for i in range(len(lista_mer)):
  #                      print(i,len(lista_mer),j,len(new_pose))
                        if lista_mery[i].pos_get==new_pose[j]:
                            new_pose.remove(new_pose[j])
                            j=j-1
                    k+=1
                    if k==10:
                        break
                for i in range(len(lista_mer)):
                        if lista_mery[i].pos_get==new_pose[0]:
                            new_pose.remove(new_pose[0])
                if len(new_pose)==0:
                    pass
                else:
                    xd=ran.randint(0,len(new_pose)-1)
                    
                    x=new_pose[xd]
                    for i in range (len(lista_mer)):
                        if i%2==0:
                            for op in range(len(uklad)):
                                for opa in range (len(uklad)):
                                    if lista_mery[i].pos_get==uklad[op][opa].pos_get:
                                        q=uklad[op][opa].pos_get
                            p1=x[0]
                            p2=x[1]
                            lista_mery[i]=uklad[p1][p2]
                        else:
                            for op in range(len(uklad)):
                               for opa in range (len(uklad)):
                                   if lista_mery[i].pos_get==uklad[op][opa].pos_get:
                                       x=uklad[op][opa].pos_get
    
                            p1=q[0]
                            p2=q[1]
                            lista_mery[i]=uklad[p1][p2]
                for i in range(len(lista_mer)):
                    lista_mer[i]=lista_mery[len(lista_mer)-1-i]
        
        else:
            # ruch po skosie w srodku
            y=ran.randint(1,2)
           # y=2
#            print(lista_mer[x].pos_get)
            if y==1:
                a=lista_mer[x-1]
                b=lista_mer[x+1]
                if a.pos_get[0]==lista_mer[x].pos_get[0]:
                    if b.pos_get[1]==lista_mer[x].pos_get[1]:
                        blol=b.pos_get[0]
                        alol=a.pos_get[1]
                        lista_mer[x]=uklad[blol][alol]
#                        print(lista_mer[x].pos_get,'po1')
                        
                elif a.pos_get[1]==lista_mer[x].pos_get[1]:
                    if b.pos_get[0]==lista_mer[x].pos_get[0]:
                        blol=b.pos_get[1]
                        alol=a.pos_get[0]
                        lista_mer[x]=uklad[alol][blol]
#                        print(lista_mer[x].pos_get,'po2')
                else:
                    pass
            else:
                #ruch "wnęki"
                if x>1 and x<len(lista_mer)-2:
                    j=x-2
                    dziala=0
                    for i in range (5):
                         if (j+i+2)>len(lista_mer):
                             pass
                         else:
#                             print(i+j+1,i,j)
                             a=lista_mer[(j+i)-1]
                             b=lista_mer[(j+i)+1]
                             if a.pos_get[0]==lista_mer[x].pos_get[0]:
                                 if b.pos_get[1]==lista_mer[x].pos_get[1]:                        
                                     dziala+=1
                            
                             elif a.pos_get[1]==lista_mer[x].pos_get[1]:
                                 if b.pos_get[0]==lista_mer[x].pos_get[0]:
                                     dziala+=1
                             else:
                                 pass
                    if dziala >=4:
                        
                        #poziom/ x blizej
                        if lista_mer[x].pos_get[0]==lista_mer[x+1].pos_get[0]:
                            if lista_mer[x-1].pos_get[1]==lista_mer[x].pos_get[1] and lista_mer[x+1].pos_get[1]==lista_mer[x+2].pos_get[1] and lista_mer[x+2].pos_get[0]== lista_mer[x-1].pos_get[0] :
                                if lista_mer[x-1].pos_get[0]>lista_mer[x].pos_get[0]:
                                    nowe_poz=[[lista_mer[x].pos_get[0]+2,lista_mer[x].pos_get[1]],[lista_mer[x+1].pos_get[0]+2,lista_mer[x+1].pos_get[1]]]
                                else:
                                    nowe_poz=[[lista_mer[x].pos_get[0]-2,lista_mer[x].pos_get[1]],[lista_mer[x+1].pos_get[0]-2,lista_mer[x+1].pos_get[1]]]
                                for i in range(len(lista_mer)):
                                    for j in range(2):
                                        if lista_mer[i].pos_get==nowe_poz[j]:
                                            break
                                
                                for i in range(2):
                                    temp=lista_mer[x+i].pos_get
                                    E1=potencjal.calc_en(lista_mer)
                                    p1=nowe_poz[i][0]
                                    p2=nowe_poz[i][1]
                                    lista_mer[x+i]=uklad[p1][p2]
                                    E2=potencjal.calc_en(lista_mer)
                                    if E2<E1:
                                        pass
                                    else:
                                        accept=ran.random
                                        if accept> np.exp*(-(E1-E2)/T):
                                            pass
                                        else:
                                           lista_mer[x+i]=uklad[temp[0]][temp[1]]
                     # pion/ x blizej
                        elif lista_mer[x].pos_get[1]==lista_mer[x+1].pos_get[1]:
                            if lista_mer[x-1].pos_get[0]==lista_mer[x].pos_get[0] and lista_mer[x+1].pos_get[0]==lista_mer[x+2].pos_get[0] and lista_mer[x+2].pos_get[1]== lista_mer[x-1].pos_get[1] :
                                if lista_mer[x-1].pos_get[1]>lista_mer[x].pos_get[1]:
                                    nowe_poz=[[lista_mer[x].pos_get[0],lista_mer[x].pos_get[1]+2],[lista_mer[x+1].pos_get[0],lista_mer[x+1].pos_get[1]+2]]
                                else:
                                    nowe_poz=[[lista_mer[x].pos_get[0],lista_mer[x].pos_get[1]-2],[lista_mer[x+1].pos_get[0],lista_mer[x+1].pos_get[1]-2]]
                                for i in range(len(lista_mer)):
                                    for j in range(2):
                                        if lista_mer[i].pos_get==nowe_poz[j]:
                                            break
                                for i in range(2):
                                    temp=lista_mer[x+i].pos_get
                                    E1=potencjal.calc_en(lista_mer)
                                    p1=nowe_poz[i][0]
                                    p2=nowe_poz[i][1]
                                    lista_mer[x+i]=uklad[p1][p2]
                                    E2=potencjal.calc_en(lista_mer)
                                    if E2<E1:
                                        pass
                                    else:
                                        accept=ran.random
                                        if accept> np.exp*(-(E1-E2)/T):
                                            pass
                                        else:
                                           lista_mer[x+i]=uklad[temp[0]][temp[1]]
                        #poziom x dalej
                        elif lista_mer[x].pos_get[0]==lista_mer[x-1].pos_get[0]:
                            if lista_mer[x+1].pos_get[1]==lista_mer[x].pos_get[1] and lista_mer[x-1].pos_get[1]==lista_mer[x-2].pos_get[1] and lista_mer[x-2].pos_get[0]== lista_mer[x+1].pos_get[0] :
                                if lista_mer[x+1].pos_get[0]>lista_mer[x].pos_get[0]:
                                    nowe_poz=[[lista_mer[x].pos_get[0]+2,lista_mer[x].pos_get[1]],[lista_mer[x-1].pos_get[0]+2,lista_mer[x-1].pos_get[1]]]
                                else:
                                    nowe_poz=[[lista_mer[x].pos_get[0]-2,lista_mer[x].pos_get[1]],[lista_mer[x-1].pos_get[0]-2,lista_mer[x-1].pos_get[1]]]
                                for i in range(len(lista_mer)):
                                    for j in range(2):
                                        if lista_mer[i].pos_get==nowe_poz[j]:
                                            break
                                for i in range(2):
                                    temp=lista_mer[x-i].pos_get
                                    E1=potencjal.calc_en(lista_mer)
                                    p1=nowe_poz[i][0]
                                    p2=nowe_poz[i][1]
                                    lista_mer[x-i]=uklad[p1][p2]
                                    E2=potencjal.calc_en(lista_mer)
                                    if E2<E1:
                                        pass
                                    else:
                                        accept=ran.random
                                        if accept> np.exp*(-(E1-E2)/T):
                                            pass
                                        else:
                                           lista_mer[x-i]=uklad[temp[0]][temp[1]]
                        #pion x dalej
                        elif lista_mer[x].pos_get[1]==lista_mer[x-1].pos_get[1]:
                            if lista_mer[x+1].pos_get[0]==lista_mer[x].pos_get[0] and lista_mer[x-1].pos_get[0]==lista_mer[x-2].pos_get[0] and lista_mer[x-2].pos_get[1]== lista_mer[x+1].pos_get[1] :
                                if lista_mer[x+1].pos_get[1]>lista_mer[x].pos_get[1]:
                                    nowe_poz=[[lista_mer[x].pos_get[0],lista_mer[x].pos_get[1]+2],[lista_mer[x-1].pos_get[0],lista_mer[x-1].pos_get[1]+2]]
                                else:
                                    nowe_poz=[[lista_mer[x].pos_get[0],lista_mer[x].pos_get[1]-2],[lista_mer[x-1].pos_get[0],lista_mer[x-1].pos_get[1]-2]]
                                for i in range(len(lista_mer)):
                                    for j in range(2):
                                        if lista_mer[i].pos_get==nowe_poz[j]:
                                            break
                                for i in range(2):
                                    temp=lista_mer[x-i].pos_get
                                    E1=potencjal.calc_en(lista_mer)
                                    p1=nowe_poz[i][0]
                                    p2=nowe_poz[i][1]
                                    lista_mer[x-i]=uklad[p1][p2]
                                    E2=potencjal.calc_en(lista_mer)
                                    if E2<E1:
                                        pass
                                    else:
                                        accept=ran.random
                                        if accept> np.exp*(-(E1-E2)/T):
                                            pass
                                        else:
                                           lista_mer[x-i]=uklad[temp[0]][temp[1]]                        
                        else:
                            pass
        return lista_mer
    
class Symulacja():
    def __init__(self,uklad,potencjal,ruch,T,steps=40):
        self.__uklad=uklad
        self.__ruch=ruch
        self.__potencjal=potencjal
        self.__T= T
        self.__steps=steps
        
    def run(self):
        lista_mer=self.__uklad.mer_get
        Ecalk=[]
        uklad=self.__uklad
        pot=self.__potencjal
        T=self.__T
#        print("ola1")
        for i in range(self.__steps):
            x=ran.randint(0,len(lista_mer)-1)
#            x=ran.randint(1,len(lista_mer)-2)
            
            mery=self.__ruch.ruchy(lista_mer,uklad,x,pot,T)
            """pokazywanie po kazdym ruchu polimeru"""
#            popo=[]
#            q=[]
#            d=[]
#            l=[]
#            for i in range(len(mery)):
#                if i==x:
#                    pass
#                elif i==x-1 or i==x+1:
#                    d.append(mery[i].pos_get[0])
#                    l.append(mery[i].pos_get[1])
#                else:
#                    popo.append(mery[i].pos_get[0])
#                    q.append(mery[i].pos_get[1])
#            plt.plot(popo,q,'ro')
#            plt.plot(mery[x].pos_get[0],mery[x].pos_get[1],'go')
#            plt.plot(d,l,'bo')
#            plt.axis([0,40,0,40])
#            plt.show()
            Ecalk.append(self.__potencjal.calc_en(mery))
#            print(Ecalk)
        return Ecalk
    
    
if __name__ == "__main__":
    x=40
    kulki=[]
    for i in range(x**2):
        kulka1 = Kulka([1])
        kulki.append(kulka1)
    uklad = Uklad(kulki,10)
    E_list=[]
    T_list=[]
    potencjal=Macierz_powiazan()
    przemieszczenie=Ruch()
    for T in range(200,250,5):
 #       print(uklad,potencjal,przemieszczenie,T)
        s=Symulacja(uklad,potencjal,przemieszczenie,T)
        E=s.run()
#        print(E)
        Esr=sum(E)/len(E)
        E_list.append(Esr)
        T_list.append(T)
    plt.plot(T_list,E_list)
    plt.show()
