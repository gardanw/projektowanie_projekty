# -*- coding: utf-8 -*-
import random
import math
import matplotlib.pyplot as plt
class Spin():
    def __init__(self, pol,spin=0,temp_spin=0, m=1,bound=[],e=[0,0,0,0]):
        self.__polozenie = [pol]
        self.__masa = m
        self.__spin=spin
        self.__temp_spin=temp_spin
        self.__bound = bound
        self.__e=e
 

   
    @property
    def e_get(self):
        e = self.__e
        return e

    def e_set(self,i, new_e):
        self.__e[i]=new_e
    
    
    @property
    def spin_get(self):
        spin = self.__spin
        return spin

    def spin_set(self,nowy_spin):
        self.__spin=nowy_spin

    @property
    def temp_spin_get(self):
        spin = self.__temp_spin
        return spin

    def temp_spin_set(self,nowy_temp_spin):
        self.__temp_spin=nowy_temp_spin

    @property
    def pos_get(self):
        pozycja = self.__polozenie[-1]
        return pozycja
    
    @property
    def bound_get(self):
        bound = self.__bound
        return bound

    def bound_set(self, new_bound):
        self.__bound=new_bound

    def pos_set(self, new_pos):
        self.__polozenie.append(new_pos)
    
    # sprawdzam jak przemieszcza sie kulka
    @property
    def pos_get_all(self):
        pozycja = self.__polozenie
        return pozycja
    




class Uklad():
    def __init__(self, lista_spin):
        self.__lista_spin = lista_spin
        self.__powiazania =[]
        self.__uklad_kul=[]
        self.__n=int((len(lista_spin))**(1/2))
      
        for i in range(self.__n):
            helper=[]
            for j in range(self.__n):
                helper.append(0)
            self.__powiazania.append(helper)
        self.__uklad=[]   
# tworzenie sieci n x n
        for i in range (self.__n):
            pomoc=[]
            for j in range (self.__n):
                k=self.__n*i+j
                pomoc.append(self.__lista_spin[k])
            self.__uklad.append(pomoc)
            
# okreslanie wiezi w sieci n x n z zamknieciem przestrzeni w kolejnosci [gora,lewo,prawo,dol]
        for i in range (self.__n):
            for j in range (self.__n):
                if i==0 and j>0 and j<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[self.__n-1,j],[i,j-1],[i,j+1],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    spin=random.randint(0,1)
                    if spin==0:
                        spin=-1
                    self.__uklad[i][j].spin_set(spin)
                    
                elif j==0 and i>0 and i<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,self.__n-1],[i,j+1],[i+1,j]]) 
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    spin=random.randint(0,1)
                    if spin==0:
                        spin=-1
                    self.__uklad[i][j].spin_set(spin)
                    
                elif j==(self.__n-1) and i>0 and i<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,j-1],[i,0],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    spin=random.randint(0,1)
                    if spin==0:
                        spin=-1
                    self.__uklad[i][j].spin_set(spin)
                    
                elif i==(self.__n-1) and j>0 and j<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,j-1],[i,j+1],[0,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    spin=random.randint(0,1)
                    if spin==0:
                        spin=-1
                    self.__uklad[i][j].spin_set(spin)
                    
                elif j==0 and i==0:
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[self.__n-1,j],[i,self.__n-1],[i,j+1],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    spin=random.randint(0,1)
                    if spin==0:
                        spin=-1
                    self.__uklad[i][j].spin_set(spin)

                elif j==0 and i==(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,self.__n-1],[i,j+1],[0,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    spin=random.randint(0,1)
                    if spin==0:
                        spin=-1
                    self.__uklad[i][j].spin_set(spin)
                    
                elif i==0 and j==(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[self.__n-1,j],[i,j-1],[i,0],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    spin=random.randint(0,1)
                    if spin==0:
                        spin=-1
                    self.__uklad[i][j].spin_set(spin)
                    
                elif i==(self.__n-1) and j==(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,j-1],[i,0],[0,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    spin=random.randint(0,1)
                    if spin==0:
                        spin=-1
                    self.__uklad[i][j].spin_set(spin)
                    
                else:
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,j-1],[i,j+1],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    spin=random.randint(0,1)
                    if spin==0:
                        spin=-1
                    self.__uklad[i][j].spin_set(spin)
        

    @property    
    def powiazania_get(self):
        powiazania = self.__powiazania
        return powiazania
    @property    
    def T_get(self):
        t = self.__T
        return t

  
    @property    
    def uklad_get(self):
        ukl = self.__uklad
        return ukl
   
    @property    
    def l_spin_get(self):
        kul = self.__lista_spin
        return kul
    
    
class Zmiany():
    def __init__(self):
        self.__a=1
        
    def delta_en(uklad):
        ukl=uklad.uklad_get
        a=random.randint(0,len(ukl)-1)
        b=random.randint(0,len(ukl)-1)
        
        ukl[a][b].temp_spin_set(ukl[a][b].spin_get)
        ukl[a][b].spin_set(-ukl[a][b].spin_get)
        l=uklad.powiazania_get[a][b]
        for k in range(4):
            x,y=l[k]
            ukl[x][y].e_set(3-k,-ukl[x][y].e_get[3-k])
   # [gora,lewo,prawo,dol]
    
    def calc_en(uklad):
        Ecalk=0
        ukl=uklad.uklad_get
        for i in range (len(ukl)):
            for j in range(len(ukl)): 
                for k in range(4):    
                    a,b=uklad.powiazania_get[i][j][k]
                    ukl[i][j].e_set(k,ukl[a][b].spin_get)
                Ecalk+=sum(ukl[i][j].e_get)
        return Ecalk
    
    def ruch(E1,E2,t):
        if E1>E2:
            return E2
        else:
            y=random.random()
            x=math.exp(-(E1-E2)/t)
            if x>y:
                return E2
            else:
                return E1
        
        
        
        
    @property
    def a_get(self):
        a = self.__a
        return a

    def a_set(self,nowy_a):
        self.__a=nowy_a

    @property
    def b_get(self):
        b = self.__b
        return b

    def b_set(self,nowy_b):
        self.__b=nowy_b
        
class Symulacje():
    def __init__(self, uklad,T, steps = 100):
        self.__uklad = uklad
        self.__steps = steps
        self.__T=T
    def run(self):
        E=[]
        for i in range(1,self.__steps+1):
            E1=Zmiany.calc_en(self.__uklad)
            w=Zmiany.delta_en(self.__uklad)
            E2=Zmiany.calc_en(self.__uklad)
            new_e=Zmiany.ruch(E1,E2,self.__T)
            E.append(new_e)
        return E
            
            
if __name__ == "__main__":
    kulki=[]
    x=5
    #x=int(input("wpisz długosc boku siatki: "))
    for i in range(x**2):
        kulka1 = Spin([1])
        kulki.append(kulka1)
    uklad = Uklad(kulki)
    E=[]
    T=[]
    for i in range(300,450,5):
        s=Symulacje(uklad,i)
        x=s.run()
        E.append(sum(x)/len(x))
        T.append(i)
    plt.plot(T,E)
    plt.show()