# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import random as ran
import numpy as np
import math
import matplotlib.pyplot as plt

class Kulka():
    def __init__(self, pol, m=0.01,bound=[]):
        self.__polozenie = [pol]
        self.__masa = m
        self.__bound = bound        
        x=ran.randint(0,1)
        if x==0:
            self.__spin=-1
        else:
            self.__spin=1
    
    @property
    def spin_get(self):
        spin=self.__spin
        return spin
    def spin_set(self, new_spin):
        self.__bound=new_spin

    @property
    def pos_get(self):
        pozycja = self.__polozenie[-1]
        return pozycja
    
    @property
    def bound_get(self):
        bound = self.__bound
        return bound

    def bound_set(self, new_bound):
        self.__bound=new_bound

    def bound_rm(self,rm_pose):
        self.__bound=self.__bound.remove(rm_pose)

    def pos_set(self, new_pos):
        self.__polozenie.append(new_pos)
    
    # sprawdzam jak przemieszcza sie kulka
    @property
    def pos_get_all(self):
        pozycja = self.__polozenie
        return pozycja

class Uklad():
    def __init__(self, lista_kulek,m):
        self.__m=m #dlugosc polimeru
        self.__lista_mer=[]
        self.__lista_kulek = lista_kulek
        self.__powiazania =[]
        self.__n=int((len(lista_kulek))**(1/2))
        for i in range(self.__n):
            helper=[]
            for j in range(self.__n):
                helper.append(0)
            self.__powiazania.append(helper)
        self.__uklad=[]   
# tworzenie sieci n x n
        for i in range (self.__n):
            pomoc=[]
            for j in range (self.__n):
                k=self.__n*i+j
                pomoc.append(self.__lista_kulek[k])
            self.__uklad.append(pomoc)
            
# okreslanie wiezi w sieci n x n z zamknieciem przestrzeni
        
        for i in range (self.__n):
            for j in range (self.__n):
                if i==0 and j>0 and j<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[self.__n-1,j],[i,j-1],[i,j+1],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif j==0 and i>0 and i<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,self.__n-1],[i,j+1],[i+1,j]]) 
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    
                elif j==(self.__n-1) and i>0 and i<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,j-1],[i,0],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif i==(self.__n-1) and j>0 and j<(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,j-1],[i,j+1],[0,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif j==0 and i==0:
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[self.__n-1,j],[i,self.__n-1],[i,j+1],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
                    

                elif j==0 and i==(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,self.__n-1],[i,j+1],[0,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif i==0 and j==(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[self.__n-1,j],[i,j-1],[i,0],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                elif i==(self.__n-1) and j==(self.__n-1):
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,j-1],[i,0],[0,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get

                else:
                    self.__uklad[i][j].pos_set([i,j])
                    self.__uklad[i][j].bound_set([[i-1,j],[i,j-1],[i,j+1],[i+1,j]])
                    self.__powiazania[i][j] = self.__uklad[i][j].bound_get
        #ustawienie poczatkowe polimeru
        x=ran.randint(0,self.__n-1)
        y=ran.randint(0,self.__n-1)
        self.__lista_mer.append(self.__uklad[x][y])
 
        rand_old=0
        rand=0
        #nie uwzglednia nakladania sie na siebie (moze polime na siebie nachodzic)
        for i in range(self.__m-1):
            while rand==rand_old:
                rand=ran.randint(1,4)
            if rand==1: #gora
                x=x+1
                y=y
            elif rand==2: #bok
                x=x
                y=y-1
            elif rand==3: #bok2
                x=x
                y=y+1
            else: #dol
                x=x-1
                y=y
            if x>self.__n-1 or y>self.__n-1:
                x=self.__n%x
                y=self.__n%y
            self.__lista_mer.append(self.__uklad[x][y])
            rand_old=rand
    
    
    @property    
    def powiazania_get(self):
        powiazania = self.__powiazania
        return powiazania
    
    @property    
    def T_get(self):
        t = self.__T
        return t
  
    @property    
    def uklad_get(self):
        ukl = self.__uklad
        return ukl
   
    @property    
    def kulka_get(self):
        kul = self.__lista_kulek
        return kul
    
    @property    
    def mer_get(self):
        mery = self.__lista_mer
        return mery
    
    def mer_set(self,new_mer):
        self.__lista_mer=new_mer


class Energy():
    def __init__(self,uklad):
        self.__ukl=uklad.uklad_get

    def calc_en_spinow(self,lista):
        Ecalk=0
        for p in range(len(lista)):
            for k in range(4):            
                for q in range(len(lista)): 
                    a,b=lista[p].bound_get[k]
                    if self.__ukl[a][b].pos_get==lista[q].pos_get and q!=p and p!=q+1 and p!=q-1:
                        Ecalk+=self.__ukl[a][b].spin_get
        return Ecalk       
                            
class Ruch():
    def __init__(self):
        pass
    
    def ruch_spin(self,lista_mer,energy,t):
        x=ran.randint(0,len(lista_mer)-1)
        E1=energy.calc_en_spinow(lista_mer)
        if lista_mer[x].spin_get==1:
            lista_mer[x].spin_set=-1
        else:
            lista_mer[x].spin_set==1
        E2=energy.calc_en_spinow(lista_mer)
        if E1>E2:
            return E2
        else:
            y=ran.random()
            w=math.exp(-(E1-E2)/t)
            if w>y:
                return E2
            else:
                if lista_mer[x].spin_get==1:
                    lista_mer[x].spin_set=-1
                else:
                    lista_mer[x].spin_set==1
                return E1
            
    def ruchy(self,lista_mer,ukl,x,potencjal,T,ruch,E):
        uklad=ukl.uklad_get
        TEMP_mery=[]
        E2=0
        for i in range (len(lista_mer)):
            TEMP_mery.append(0)
        for i in range (len(lista_mer)):
            TEMP_mery[i]=lista_mer[i]
        if x==0 or x==len(lista_mer)-1:
            y=ran.randint(1,2)
       
            if y==1:
                # ruch ogona
                old_pose=lista_mer[x].pos_get

                    
                new_pose=[[old_pose[0],old_pose[1]-1],[old_pose[0],old_pose[1]+1],[old_pose[0]-1,old_pose[1]],[old_pose[0]+1,old_pose[1]]]
                j=2
                k=0
                while len(new_pose)>1:
                    for i in range(len(lista_mer)):
                        if lista_mer[i].pos_get==new_pose[j]:
                            new_pose.remove(new_pose[j])
                            j=j-1
                    k+=1
                    if k==10:
                        break
                for i in range(len(lista_mer)):
                        if lista_mer[i].pos_get==new_pose[0]:
                            new_pose.remove(new_pose[0])
                if len(new_pose)==0:
                    pass
                else:
                    xd=ran.randint(0,len(new_pose)-1)
                    
                    x=new_pose[xd]
                    for i in range (len(lista_mer)):
                        if i%2==0:
                            for op in range(len(uklad)):
                                for opa in range (len(uklad)):
                                    if lista_mer[i].pos_get==uklad[op][opa].pos_get:
                                        q=uklad[op][opa].pos_get
                            p1=x[0]
                            p2=x[1]
                            lista_mer[i]=uklad[p1][p2]
                        else:
                            for op in range(len(uklad)):
                               for opa in range (len(uklad)):
                                   if lista_mer[i].pos_get==uklad[op][opa].pos_get:
                                       x=uklad[op][opa].pos_get
    
                            p1=q[0]
                            p2=q[1]
                            lista_mer[i]=uklad[p1][p2]
                e=[]            
                for i in range (100):
                    e.append(ruch.ruch_spin(lista_mer,potencjal,T))
                E2=sum(e)/len(e)
                
                
            else:
                lista_mery=[]
                for i in range (len(lista_mer)):
                    lista_mery.append(0)
                for i in range (len(lista_mer)):
                    lista_mery[i]=lista_mer[len(lista_mer)-1-i]
                old_pose=lista_mery[x].pos_get              
                new_pose=[[old_pose[0],old_pose[1]-1],[old_pose[0],old_pose[1]+1],[old_pose[0]-1,old_pose[1]],[old_pose[0]+1,old_pose[1]]]
               
                #przemieszczenie mera do przodu
                j=3
                k=0
                while len(new_pose)>1:
                    for i in range(len(lista_mer)):
                        if lista_mery[i].pos_get==new_pose[j]:
                            new_pose.remove(new_pose[j])
                            j=j-1
                    k+=1
                    if k==10:
                        break
                for i in range(len(lista_mer)):
                        if lista_mery[i].pos_get==new_pose[0]:
                            new_pose.remove(new_pose[0])
                if len(new_pose)==0:
                    pass
                else:
                    xd=ran.randint(0,len(new_pose)-1)
                    
                    x=new_pose[xd]
                    for i in range (len(lista_mer)):
                        if i%2==0:
                            for op in range(len(uklad)):
                                for opa in range (len(uklad)):
                                    if lista_mery[i].pos_get==uklad[op][opa].pos_get:
                                        q=uklad[op][opa].pos_get
                            p1=x[0]
                            p2=x[1]
                            lista_mery[i]=uklad[p1][p2]
                        else:
                            for op in range(len(uklad)):
                               for opa in range (len(uklad)):
                                   if lista_mery[i].pos_get==uklad[op][opa].pos_get:
                                       x=uklad[op][opa].pos_get
    
                            p1=q[0]
                            p2=q[1]
                            lista_mery[i]=uklad[p1][p2]
                for i in range(len(lista_mer)):
                    lista_mer[i]=lista_mery[len(lista_mer)-1-i]
                e=[]            
                for i in range (100):
                    e.append(ruch.ruch_spin(lista_mer,potencjal,T))
                E2=sum(e)/len(e)
        
        else:
            # ruch po skosie w srodku
            y=ran.randint(1,2)
            if y==1:
                a=lista_mer[x-1]
                b=lista_mer[x+1]
                if a.pos_get[0]==lista_mer[x].pos_get[0]:
                    if b.pos_get[1]==lista_mer[x].pos_get[1]:
                        blol=b.pos_get[0]
                        alol=a.pos_get[1]
                        lista_mer[x]=uklad[blol][alol]
                        
                elif a.pos_get[1]==lista_mer[x].pos_get[1]:
                    if b.pos_get[0]==lista_mer[x].pos_get[0]:
                        blol=b.pos_get[1]
                        alol=a.pos_get[0]
                        lista_mer[x]=uklad[alol][blol]
                else:
                    pass
                e=[]            
                for i in range (100):
                    e.append(ruch.ruch_spin(lista_mer,potencjal,T))
                E2=sum(e)/len(e)
            else:
                #ruch "wnęki"
                if x>1 and x<len(lista_mer)-2:
                    j=x-2
                    dziala=0
                    for i in range (5):
                         if (j+i+2)>len(lista_mer):
                             pass
                         else:
                             a=lista_mer[(j+i)-1]
                             b=lista_mer[(j+i)+1]
                             if a.pos_get[0]==lista_mer[x].pos_get[0]:
                                 if b.pos_get[1]==lista_mer[x].pos_get[1]:                        
                                     dziala+=1
                            
                             elif a.pos_get[1]==lista_mer[x].pos_get[1]:
                                 if b.pos_get[0]==lista_mer[x].pos_get[0]:
                                     dziala+=1
                             else:
                                 pass
                    if dziala >=4:
                        
                        #poziom/ x blizej
                        if lista_mer[x].pos_get[0]==lista_mer[x+1].pos_get[0]:
                            if lista_mer[x-1].pos_get[1]==lista_mer[x].pos_get[1] and lista_mer[x+1].pos_get[1]==lista_mer[x+2].pos_get[1] and lista_mer[x+2].pos_get[0]== lista_mer[x-1].pos_get[0] :
                                if lista_mer[x-1].pos_get[0]>lista_mer[x].pos_get[0]:
                                    nowe_poz=[[lista_mer[x].pos_get[0]+2,lista_mer[x].pos_get[1]],[lista_mer[x+1].pos_get[0]+2,lista_mer[x+1].pos_get[1]]]
                                else:
                                    nowe_poz=[[lista_mer[x].pos_get[0]-2,lista_mer[x].pos_get[1]],[lista_mer[x+1].pos_get[0]-2,lista_mer[x+1].pos_get[1]]]
                                for i in range(len(lista_mer)):
                                    for j in range(2):
                                        if lista_mer[i].pos_get==nowe_poz[j]:
                                            break
                                
                                for i in range(2):
                                    temp=lista_mer[x+i].pos_get
                                    E1=potencjal.calc_en(lista_mer)
                                    p1=nowe_poz[i][0]
                                    p2=nowe_poz[i][1]
                                    lista_mer[x+i]=uklad[p1][p2]
                                    E2=potencjal.calc_en(lista_mer)
                                    if E2<E1:
                                        pass
                                    else:
                                        accept=ran.random
                                        if accept> np.exp*(-(E1-E2)/T):
                                            pass
                                        else:
                                           lista_mer[x+i]=uklad[temp[0]][temp[1]]
                            e=[]            
                            for i in range (100):
                                e.append(ruch.ruch_spin(lista_mer,potencjal,T))
                            E2=sum(e)/len(e)
                     # pion/ x blizej
                        elif lista_mer[x].pos_get[1]==lista_mer[x+1].pos_get[1]:
                            if lista_mer[x-1].pos_get[0]==lista_mer[x].pos_get[0] and lista_mer[x+1].pos_get[0]==lista_mer[x+2].pos_get[0] and lista_mer[x+2].pos_get[1]== lista_mer[x-1].pos_get[1] :
                                if lista_mer[x-1].pos_get[1]>lista_mer[x].pos_get[1]:
                                    nowe_poz=[[lista_mer[x].pos_get[0],lista_mer[x].pos_get[1]+2],[lista_mer[x+1].pos_get[0],lista_mer[x+1].pos_get[1]+2]]
                                else:
                                    nowe_poz=[[lista_mer[x].pos_get[0],lista_mer[x].pos_get[1]-2],[lista_mer[x+1].pos_get[0],lista_mer[x+1].pos_get[1]-2]]
                                for i in range(len(lista_mer)):
                                    for j in range(2):
                                        if lista_mer[i].pos_get==nowe_poz[j]:
                                            break
                                for i in range(2):
                                    temp=lista_mer[x+i].pos_get
                                    E1=potencjal.calc_en(lista_mer)
                                    p1=nowe_poz[i][0]
                                    p2=nowe_poz[i][1]
                                    lista_mer[x+i]=uklad[p1][p2]
                                    E2=potencjal.calc_en(lista_mer)
                                    if E2<E1:
                                        pass
                                    else:
                                        accept=ran.random
                                        if accept> np.exp*(-(E1-E2)/T):
                                            pass
                                        else:
                                           lista_mer[x+i]=uklad[temp[0]][temp[1]]
                            e=[]            
                            for i in range (100):
                                e.append(ruch.ruch_spin(lista_mer,potencjal,T))
                            E2=sum(e)/len(e)
                        #poziom x dalej
                        elif lista_mer[x].pos_get[0]==lista_mer[x-1].pos_get[0]:
                            if lista_mer[x+1].pos_get[1]==lista_mer[x].pos_get[1] and lista_mer[x-1].pos_get[1]==lista_mer[x-2].pos_get[1] and lista_mer[x-2].pos_get[0]== lista_mer[x+1].pos_get[0] :
                                if lista_mer[x+1].pos_get[0]>lista_mer[x].pos_get[0]:
                                    nowe_poz=[[lista_mer[x].pos_get[0]+2,lista_mer[x].pos_get[1]],[lista_mer[x-1].pos_get[0]+2,lista_mer[x-1].pos_get[1]]]
                                else:
                                    nowe_poz=[[lista_mer[x].pos_get[0]-2,lista_mer[x].pos_get[1]],[lista_mer[x-1].pos_get[0]-2,lista_mer[x-1].pos_get[1]]]
                                for i in range(len(lista_mer)):
                                    for j in range(2):
                                        if lista_mer[i].pos_get==nowe_poz[j]:
                                            break
                                for i in range(2):
                                    temp=lista_mer[x-i].pos_get
                                    E1=potencjal.calc_en(lista_mer)
                                    p1=nowe_poz[i][0]
                                    p2=nowe_poz[i][1]
                                    lista_mer[x-i]=uklad[p1][p2]
                                    E2=potencjal.calc_en(lista_mer)
                                    if E2<E1:
                                        pass
                                    else:
                                        accept=ran.random
                                        if accept> np.exp*(-(E1-E2)/T):
                                            pass
                                        else:
                                           lista_mer[x-i]=uklad[temp[0]][temp[1]]
                            e=[]            
                            for i in range (100):
                                e.append(ruch.ruch_spin(lista_mer,potencjal,T))
                            E2=sum(e)/len(e)
                        #pion x dalej
                        elif lista_mer[x].pos_get[1]==lista_mer[x-1].pos_get[1]:
                            if lista_mer[x+1].pos_get[0]==lista_mer[x].pos_get[0] and lista_mer[x-1].pos_get[0]==lista_mer[x-2].pos_get[0] and lista_mer[x-2].pos_get[1]== lista_mer[x+1].pos_get[1] :
                                if lista_mer[x+1].pos_get[1]>lista_mer[x].pos_get[1]:
                                    nowe_poz=[[lista_mer[x].pos_get[0],lista_mer[x].pos_get[1]+2],[lista_mer[x-1].pos_get[0],lista_mer[x-1].pos_get[1]+2]]
                                else:
                                    nowe_poz=[[lista_mer[x].pos_get[0],lista_mer[x].pos_get[1]-2],[lista_mer[x-1].pos_get[0],lista_mer[x-1].pos_get[1]-2]]
                                for i in range(len(lista_mer)):
                                    for j in range(2):
                                        if lista_mer[i].pos_get==nowe_poz[j]:
                                            break
                                for i in range(2):
                                    temp=lista_mer[x-i].pos_get
                                    E1=potencjal.calc_en(lista_mer)
                                    p1=nowe_poz[i][0]
                                    p2=nowe_poz[i][1]
                                    lista_mer[x-i]=uklad[p1][p2]
                                    E2=potencjal.calc_en(lista_mer)
                                    if E2<E1:
                                        pass
                                    else:
                                        accept=ran.random
                                        if accept> np.exp*(-(E1-E2)/T):
                                            pass
                                        else:
                                           lista_mer[x-i]=uklad[temp[0]][temp[1]]                        
                            e=[]            
                            for i in range (100):
                                e.append(ruch.ruch_spin(lista_mer,potencjal,T))
                            E2=sum(e)/len(e)
                        else:
                            pass
        if E2<E:
             return lista_mer
        else:
            y=ran.random(0,1)
            zmien=math.exp(-(E-E2)/T)
            if zmien>y:
                return lista_mer
            else:
                return TEMP_mery
               
       

class Symulacja():
    def __init__(self,uklad,ruch,energy,T,steps=50):
        self.__uklad=uklad
        self.__ruch=ruch
        self.__step=steps
        self.__energy=energy
        self.__T=T
   
    def run(self):
        lista_mer=self.__uklad.mer_get
        Ecalk=[]
        E=1000000
        for i in range(self.__step):
            x=ran.randint(1,len(lista_mer)-1)
            mery=self.__ruch.ruchy(lista_mer,self.__uklad,x,self.__energy,self.__T,self.__ruch,E)
            p=self.__energy.calc_en_spinow(mery)
#            print(p)
            Ecalk.append(p)
            
        return Ecalk
    
    
if __name__ == "__main__":
    x=40
    kulki=[]
    for i in range(x**2):
        kulka1 = Kulka([1])
        kulki.append(kulka1)
    uklad = Uklad(kulki,10)
    
    for i in range(200,220,5):
        pot=Energy(uklad)
        ruch=Ruch()
        s=Symulacja(uklad,ruch,pot,i)
        E=s.run()
    plt.plot(E)
    plt.show()
